<?php

namespace Pondit\Library;

class Book
{

    public function __construct($book_name)
    {
        $this->book_name = $book_name;
    }


    public function book()
    {
        $result = "Book Name: " . $this->book_name;
        return $result;

    }
}
