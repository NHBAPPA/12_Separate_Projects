<?php
require_once 'vendor/autoload.php';

use Pondit\Calculator\VolumeCalculator\Cylinder;
use Pondit\Calculator\VolumeCalculator\Cone;
use Pondit\Calculator\VolumeCalculator\Volume;


$volume = new Volume();
echo "<hr>";

$cylinder = new Cylinder(3.1416, 15, 6.5);
$result = $cylinder->cylinder();
echo $result;
echo "<hr>";

$cone = new Cone (3.1416, 12, 5);
$result = $cone->cone();
echo $result;