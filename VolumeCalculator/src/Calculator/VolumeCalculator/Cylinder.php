<?php

namespace Pondit\Calculator\VolumeCalculator;


class Cylinder
{
    public function __construct($pi, $radius, $height)
    {
        $this->pi = $pi;
        $this->radius = $radius;
        $this->height = $height;
    }

    public function cylinder()
    {
        $result = 2 * $this->pi * $this->radius * ($this->radius + $this->height);
        return $result;
    }
}