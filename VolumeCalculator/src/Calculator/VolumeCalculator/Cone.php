<?php

namespace Pondit\Calculator\VolumeCalculator;

class Cone
{
    public function __construct($pi, $diameter, $height)
    {
        $this->pi = $pi;
        $this->diameter = $diameter;
        $this->height = $height;
    }

    public function cone()
    {

        $result = 1 / 3 * $this->height * $this->pi * (($this->diameter / 2) ^ 2);
        return $result;
    }
}