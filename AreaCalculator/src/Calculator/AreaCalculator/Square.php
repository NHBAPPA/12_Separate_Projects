<?php

namespace Pondit\Calculator\AreaCalculator;

class Square
{
    public function __construct($length,$width)
    {
        $this->length=$length;
        $this->width=$width;
    }

    public function square(){

       $result=$this->length**$this->width;
       return $result;
    }
}