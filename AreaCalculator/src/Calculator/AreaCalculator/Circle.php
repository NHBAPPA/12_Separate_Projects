<?php

namespace Pondit\Calculator\AreaCalculator;

class Circle
{
    public function __construct($pi,$radius)
    {
        $this->pi=$pi;
        $this->radius=$radius;
    }
    public function circle()
    {
        $result= $this->pi*$this->radius*$this->radius;
        return $result;
    }
}