<?php

namespace Pondit\Calculator\AreaCalculator;


class Triangle
{
    public function __construct($width,$length)
    {
        $this->width=$width;
        $this->length=$length;
    }
    public function triangle()
    {
        $result= ($this->length*$this->width)/2;
        return $result;
    }
}