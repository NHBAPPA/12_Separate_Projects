<?php
require_once 'vendor/autoload.php';

use Pondit\Calculator\AreaCalculator\Rectangle;
use Pondit\Calculator\AreaCalculator\Square;
use Pondit\Calculator\AreaCalculator\Triangle;
use Pondit\Calculator\AreaCalculator\Circle;

$rectangle= new Rectangle(100,200);
$area = $rectangle->rectangle();
echo $area;
echo "<hr>";

$square= new Square(10,2);
$result= $square->square();
echo $result;
echo "<hr>";

$triangle= new Triangle(10,3);
$result= $triangle->triangle();
echo $result;
echo "<hr>";

$circle= new Circle(3.1416,4);
$result= $circle->circle();
echo $result;