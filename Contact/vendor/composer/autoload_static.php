<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInit7b8a42e43f73d6bc68afd9dca51175d2
{
    public static $prefixLengthsPsr4 = array (
        'p' => 
        array (
            'pondit\\' => 7,
        ),
    );

    public static $prefixDirsPsr4 = array (
        'pondit\\' => 
        array (
            0 => __DIR__ . '/../..' . '/src',
        ),
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->prefixLengthsPsr4 = ComposerStaticInit7b8a42e43f73d6bc68afd9dca51175d2::$prefixLengthsPsr4;
            $loader->prefixDirsPsr4 = ComposerStaticInit7b8a42e43f73d6bc68afd9dca51175d2::$prefixDirsPsr4;

        }, null, ClassLoader::class);
    }
}
