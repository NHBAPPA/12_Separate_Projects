<?php

namespace Pondit\Calculator\NumberCalculator;

class Addition
{
    public function addnumbers ($number1, $number2)
    {
        $result = $number1 + $number2;
        return $result;
    }
}